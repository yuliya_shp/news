//
//  TableViewController.swift
//  NewsApp
//
//
//

import UIKit
import RealmSwift

class TableViewController: UITableViewController, UISearchResultsUpdating {
    
    let realm = try! Realm()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var today: String!
    var secondDay: String!
    
    
    private var articles: [Article]? = []
    private var filteredArticles: [Article] = []
    var articlesRealm: Results<ArticleRealm>?
    
    var resultSearchController = UISearchController()
    
    var isLoading = false
    var isSaved = false
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        today = getDate()
        secondDay = getSecondDay()
        self.tableView.rowHeight = 138
        
        articlesRealm = realm.objects(ArticleRealm.self)
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        definesPresentationContext = true

        fetchData(refresh: false)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
             return filteredArticles.count
        } else if isSaved {
            return articlesRealm?.count ?? 0
        } else {
            return self.articles?.count ?? 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        if isFiltering {
            cell.titleLabel.text = filteredArticles[indexPath.row].title
            cell.descLabel.text = filteredArticles[indexPath.row].desc
                
            if articles?[indexPath.item].urlToImage != nil {
                let image = UIImage(named: "thumbnail")
                let url = URL(string: (articles?[indexPath.item].urlToImage)!)
                let data = try? Data(contentsOf: url!)

                cell.imageLabel.image = UIImage(data: (data ?? image?.pngData())!)
            }
                return cell
        } else if isSaved {
            cell.titleLabel.text = self.articlesRealm?[indexPath.item].title
            cell.descLabel.text = self.articlesRealm?[indexPath.item].desc
            return cell
        } else {
            cell.titleLabel.text = self.articles?[indexPath.item].title
            cell.descLabel.text = self.articles?[indexPath.item].desc
            
            if articles?[indexPath.item].urlToImage != nil {
                let image = UIImage(named: "thumbnail")
                let url = URL(string: (articles?[indexPath.item].urlToImage)!)
                let data = try? Data(contentsOf: url!)
                cell.imageLabel.image = UIImage(data: (data ?? image?.pngData())!)
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        let currentArticle = articles![indexPath.row]
        
        let saveItem = UIContextualAction(style: .normal, title: "Save") { (_, _, _) in
            self.saveArticle(news: currentArticle)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        let swipeActions = UISwipeActionsConfiguration(actions: [saveItem])
        
        return swipeActions
    }
    
    @IBAction func showSavedArticles(_ sender: UIBarButtonItem) {
        isSaved = true
        self.tableView.reloadData()
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if offsetY > (contentHeight - 100 - scrollView.frame.height) {
            if !isLoading{
                beginLoadNewData()
            }
        }
    }
    
    func beginLoadNewData() {
        self.tableView.reloadData()
        if !self.isLoading {
            self.isLoading = true
            DispatchQueue.global().async {
                
                let postsUrl = URL(string: "https://newsapi.org/v2/everything?q=apple&from=2021-04-13&to=2021-04-13&apiKey=3fb18c84dc494acf8881f24bc00e400d")

                let url = URLRequest(url: postsUrl!)
                
                let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                        
                        if let articlesFromJson = json["articles"] as? [[String : AnyObject]] {
                            for articleFromJson in articlesFromJson {
                                let article = Article()
                                if let title = articleFromJson["title"] as? String {
                                    article.title = title
                                    if let desc = articleFromJson["description"] as? String {
                                        article.desc = desc
                                        if let urlToImage = articleFromJson["urlToImage"] as? String {
                                            article.urlToImage = urlToImage
                                        }
                                    }
                                }
                                self.articles?.append(article)
                            }
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    } catch let error {
                        print(error)
                    }
                }
                task.resume()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.isLoading = false
                }
            }
        }
    }
    
    // MARK: - Save data in DataBase
    
    private func saveArticle(news: Article) {
        try! realm.write({
            let newsRealm = ArticleRealm()
            newsRealm.title = news.title
            newsRealm.desc = news.desc
            realm.add(newsRealm)
            })
    }
    
    // MARK: - Refresh table
    
    @objc func refreshData() {
        fetchData(refresh: true)
    }
 
    // MARK: - Fetch API
    
    func fetchData(refresh: Bool = false) {
        
        if refresh {
            refreshControl?.beginRefreshing()
        }
        let postsUrl = URL(string: "https://newsapi.org/v2/everything?q=apple&from=\(self.today!)/&to=\(self.today!)/&apiKey=3fb18c84dc494acf8881f24bc00e400d)")
        let url = URLRequest(url: postsUrl!)

        let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            
            if error != nil {
                print(error!)
                return
            }
            self.articles = [Article]()
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                
                if let articlesFromJson = json["articles"] as? [[String : AnyObject]] {
                    for articleFromJson in articlesFromJson {
                        let article = Article()
                        if let title = articleFromJson["title"] as? String {
                            article.title = title
                            if let desc = articleFromJson["description"] as? String {
                                article.desc = desc
                                if let urlToImage = articleFromJson["urlToImage"] as? String {
                                    article.urlToImage = urlToImage
                                }
                            }
                        }
                        self.articles?.append(article)
                    }
                }
                DispatchQueue.main.async {
                    if refresh {
                        self.refreshControl?.endRefreshing()
                    }
                    self.tableView.reloadData()
                }
            } catch let error {
                print(error)
            }
            
        }
        task.resume()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredArticles = articles!.filter { (article: Article) -> Bool in
        return article.title!.lowercased().contains(searchText.lowercased())
      }
      tableView.reloadData()
    }
    
    // MARK: - Get dates
    
    func getDate () -> String {
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        let currentDate = formatter.string(from: today)
        
        return currentDate
    }
    
    func getSecondDay()-> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: Calendar.current.date(byAdding: .day, value: -1, to: Date())!)
    }
}

extension TableViewController {
    public func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
